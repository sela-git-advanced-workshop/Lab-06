# Advanced Git Workshop
Lab 06: Search contents within a Git repository

---

# Tasks

 - Searching contents within a repo

---

## Preparations

 - Let's clone a repository to work with:
 
```
$ git clone https://oauth2:gkLjhh5W4te-cJngRBxB@gitlab.com/sela-git-advanced-workshop/static-web-app.git lab6
$ cd lab6
```

---

## Searching contents within a repo

 - In which part of the repository is the word "BUG" written:
 
```
$ git grep BUG
```

 - In which files is the "jquery.min.js" used?:
 
```
$ git grep -l jquery.min.js
```

 - Which javascript files are using JQuery?:
 
```
$ git grep -l "jQuery" -- '*.js'
```

 - What are the fonts used in the application?:
 
```
$ git grep font-family -- '*freelancer.css'
```

 - Search for all the images in the repository :
 
```
$ git grep -l . -- '*.png' or '*.jpg'
```

---

# Cleanup (optional)

 - Remove the repository used during the lab:
 
```
$ cd ..
$ rm -rf lab6
```
